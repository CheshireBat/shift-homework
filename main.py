from fastapi import FastAPI, HTTPException
from core import create_access_token, TOKEN_EXPIRATION_TIME, hide_password
from models import Employee
from datetime import datetime, timedelta


app = FastAPI()

# Фэйковая база данных работников
open_db = {
    "john": Employee(id=1, name="John Smith", username="john", password="e38ad214943daad1d64c102faec29de4afe9da3d", token=False, salary=50000, next_promotion=datetime(2023, 9, 11)),
    "jane": Employee(id=2, name="Jane Smith", username="jane", password="2aa60a8ff7fcd473d321e0146afd9e26df395147", token=False, salary=60000, next_promotion=datetime(2025, 9, 8)),
    "bob": Employee(id=3, name="Bob Smith", username="bob", password="1119cfd37ee247357e034a08d844eea25f6fd20f", token=False, salary=70000, next_promotion=datetime(2024, 1, 11))
}


# Хранилище токенов
tokens = {}


@app.post("/auth")
async def authenticate_user(username: str, password: str):
    # Ищем пользователя в базе данных
    user = open_db.get(username, None)
    if user is None or user.password != hide_password(password):
        raise HTTPException(status_code=401, detail="Invalid username or password")
    # Создаем токен для пользователя
    access_token = create_access_token(user)
    # Добавляем пометку в БД
    user.token = True
    # Добавляем сам токен
    tokens[access_token] = {"exp": datetime.utcnow() + timedelta(seconds=TOKEN_EXPIRATION_TIME),
                            "salary": user.salary,
                            "next_promotion": user.next_promotion}
    # Возвращаем токен и время жизни токена
    return {"token": access_token, "expires_in": f"{TOKEN_EXPIRATION_TIME} seconds"}


@app.post("/salary")
async def get_salary(username: str, token: str):
    # Проверяем пользователя в БД и наличие токена
    user = open_db.get(username, None)
    # Проверяем наличие токена и его правильность
    if user is None or not user.token or tokens.get(token, None) is None:
        raise HTTPException(status_code=401, detail="Invalid authorization headers")
    # Проверяем, что токен еще не сгорел
    if tokens[token]["exp"] < datetime.utcnow():
        user.token = False
        del tokens[token]
        raise HTTPException(status_code=401, detail="Time is over, need new token to continue")
    # Возвращаем информацию
    return {"Your salary": tokens[token]["salary"],
            "Next promotion": tokens[token]["next_promotion"]}