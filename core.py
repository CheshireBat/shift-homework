from datetime import datetime, timedelta
from models import Employee
import hashlib
import jwt


# Это секретный ключ для подписи токена, его необходимо хранить в секрете в реальном приложении
SECRET_KEY = "mysecretkey"
# Это время жизни токена в секундах (30 секунд, для тестов)
TOKEN_EXPIRATION_TIME = 30
 

# Функция для генерации токена аутентификации
def create_access_token(user: Employee) -> str:
    # Создаем заголовок токена
    headers = {
        "alg": "HS256",
        "typ": "JWT"
    }
    # Создаем тело токена
    payload = {
        "sub": user.username,
        "exp": datetime.utcnow() + timedelta(seconds=TOKEN_EXPIRATION_TIME),
        "iat": datetime.utcnow()
    }
    # Генерируем токен
    token = jwt.encode(payload, SECRET_KEY, algorithm="HS256", headers=headers)

    return token


def hide_password(password: str) -> str:
    return hashlib.sha1(password.encode('utf-8')).hexdigest()