from pydantic import BaseModel
from typing import Optional
from datetime import datetime

class Employee(BaseModel):
    id: int
    name: str
    username: str
    password: str
    token: bool
    salary: float
    next_promotion: Optional[datetime]
    